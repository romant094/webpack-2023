import { createElement, useState } from 'react'
import { createRoot } from 'react-dom/client'

const container = document.querySelector('.app')
const root = createRoot(container)

const Menu = () => {
  const items = ['home.html', 'react.html', 'image.html']
  return createElement('div', {
    style: {
      display: 'flex',
      gap: 10,
      marginBottom: 20
    }
  }, items.map(item => createElement('a', {
    href: item,
    key: item
  }, item)))
}

const App = () => {
  const [value, setValue] = useState('This page is using React')

  return createElement('div', {
    style: {
      padding: 10,
      margin: 0
    }
  }, [
    createElement(Menu),
    createElement('input', {
      value,
      onChange: e => setValue(e.target.value),
      style: {
        width: '100%',
        padding: '7px 17px',
        fontSize: 16
      }
    }),
    createElement('h1', { style: { color: '#999' } }, value),
  ])
}

root.render(createElement(App))