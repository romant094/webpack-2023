import { Button, Heading } from './components'
import { upperCase } from 'lodash'

const button = new Button(upperCase('add a webpack paragraph'))
const heading = new Heading('Webpack is awesome')
heading.render()
button.render()