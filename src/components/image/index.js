import { Component } from '../component'
import './styles.scss'

export class Image extends Component {
  constructor(src, alt) {
    super()
    this.src = src
    this.alt = alt
  }

  render() {
    const img = document.createElement('img')
    img.src = this.src
    img.alt = this.alt
    img.classList.add('image')
    this.append(img)
  }
}