import { Component } from '../component'
import './style.scss'

export class Button extends Component {
  constructor(text) {
    super()
    this.text = text
  }

  onClick = () => {
    const p = document.createElement('p')
    p.textContent = 'Hello webpack'
    this.append(p)
  }

  render() {
    const btn = document.createElement('button')
    btn.classList.add('btn')
    btn.textContent = this.text
    btn.onclick = this.onClick
    this.append(btn)
  }
}