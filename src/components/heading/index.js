import './styles.scss'
import { Component } from '../component'

export class Heading extends Component {
  constructor(text) {
    super()
    this.text = text
  }

  render() {
    const h1 = document.createElement('h1')
    h1.textContent = this.text
    this.append(h1)
  }
}