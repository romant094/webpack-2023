import { Heading, Image } from './components'
import island from './assets/island.jpeg'

const heading = new Heading('Other page from webpack')
const image = new Image(island, '')

heading.render()
image.render()