const path = require('path')
// terser-webpack-plugin comes from webpack by default
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// Starting from webpack 5.20 is needed no more, use clean option in the config.
// But the plugin offers more features. such as removing files in different folders.
// const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const isDev = process.env.NODE_ENV === 'develop'
const isProd = !isDev
const distPath = path.resolve(__dirname, './dist')

const getFileName = (name) => {
  if (isDev) return name
  const nameArr = name.split('.')
  const lastEl = nameArr.splice(-1)[0]
  nameArr.push('[contenthash:8]')
  nameArr.push(lastEl)
  return nameArr.join('.')
}

const getHtmlPages = (config) => config.map(({
  chunkName,
  pathValue,
  ...item
}) => new HtmlWebpackPlugin({
  title: 'Webpack 2023',
  minify: isProd,
  inject: 'body',
  favicon: path.resolve(__dirname, 'public/favicon.ico'),
  description: 'The awesomeness of the webpack is really high',
  template: path.resolve(__dirname, 'public/template.hbs'),
  ...item
}))

const pages = [
  {
    filename: 'home.html',
    chunks: ['home'],
    chunkName: 'home',
    pathValue: './src/HomePage.js'
  },
  {
    filename: 'react.html',
    title: 'Webpack 2023 - React demo',
    chunks: ['react'],
    chunkName: 'react',
    pathValue: './src/ReactPage.js'
  },
  {
    filename: 'image.html',
    title: 'Webpack 2023 - Image',
    chunks: ['image'],
    chunkName: 'image',
    pathValue: './src/ImagePage.js'
  }
]

const getEntries = (arr, res = {}) => {
  if (arr.length) {
    const {
      chunkName,
      pathValue
    } = arr[0]
    return getEntries(arr.slice(1), {
      ...res,
      [chunkName]: pathValue
    })
  }

  return res
}

const plugins = [
  // new CleanWebpackPlugin(),
  ...getHtmlPages(pages)
]

if (isProd) {
  const css = new MiniCssExtractPlugin({
    filename: getFileName('[name].css'),
    chunkFilename: 'chunk-[id].css',
  })
  plugins.push(css)
}

const devServer = () => {
  const devServer = {
    port: 9000,
    static: {
      directory: distPath
    },
    devMiddleware: {
      index: 'index.html',
      writeToDisk: true
    },
    hot: true,
    open: true
  }

  return isDev && { devServer }
}

const optimization = () => {
  const optimization = {
    splitChunks: {
      chunks: 'all',
      // minSize is needed to limit the min size to be split for chunks (default, 30Kb)
      minSize: 3000
    },
  }
  return isProd && { optimization }
}

module.exports = {
  // If needed to split the code to different bundles then send object to entry instead of string
  entry: getEntries(pages),
  output: {
    filename: getFileName('[name].js'),
    path: distPath,
    publicPath: '',
    clean: true
  },
  mode: isDev ? 'development' : 'production',
  ...optimization(),
  ...devServer(),
  module: {
    rules: [
      // asset types
      // asset/resource - for big files like images (they will be copied to the dist folder)
      // asset/inline - for small files like svg-icons (they will be converted to base64 format and put into the bundle)
      // asset - webpack chooses between first two types by itself (the condition is 8Kb file size)
      // asset/source - for raw text from text files
      {
        test: /\.(png|jpg|jpeg)$/,
        type: 'asset/resource'
      },
      {
        test: /\.(svg)$/,
        type: 'asset/inline'
        // To limit max file size to load as resource use parser -> dataUrlCondition -> maxSize
        //   parser: {
        //     dataUrlCondition: {
        //       maxSize: 100
        //     }
        //   }
      },
      // To limit max file size to load as resource use parser -> dataUrlCondition -> maxSize
      // {
      //   test: /\.(svg)$/,
      //   type: 'asset',
      //   parser: {
      //     dataUrlCondition: {
      //       maxSize: 100
      //     }
      //   }
      // },
      {
        test: /\.(txt)$/,
        type: 'asset/source'
      },
      {
        test: /\.(css)$/,
        // loaders work one by one from the right
        use: [isProd ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader']
      },
      {
        test: /\.(scss)$/,
        use: [isProd ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
          }
        }
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader'
      }
    ]
  },
  plugins
}